from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt

from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import Toolbar
from gui.widgets.structure_dock_widget import StructureDockWidget
from gui.widgets.central_widget import CentralWidget
from gui.widgets.tab_widget import TabWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/blue-document.png"))
        self.resize(1000, 650)

        self.menu_bar= MenuBar(self)
        self.tool_bar= Toolbar(self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget=StructureDockWidget("Informacioni resursi", self)

        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.file_dock_widget.clicked.connect(self.centralWidget().add_tab)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        
        
        #sacuvavanje prijavljenog korisnika iz dijaloga
        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Marko Grujičić"))
    
        