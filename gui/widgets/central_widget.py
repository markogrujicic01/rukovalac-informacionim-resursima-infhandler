
from PySide2.QtWidgets import  QTabWidget,QPushButton
from PySide2.QtWidgets import  QTabWidget,QPushButton

class CentralWidget(QTabWidget):
    def __init__(self, parent):
        super().__init__(parent)
        
        self.setTabsClosable(True)
        self.tabCloseRequested.connect(self.removeTab)
    
    def tabs(self):
        tabs_list = []
        for i in range(self.count()):
            tabs_list.append(self.tabText(i))
        return tabs_list

    def get_index(self, tab_name):
        try:
            return self.tabs.index(tab_name)
        except ValueError:
            return None

    def add_tab(self, data_name, data_type):
        tab_name = get_tab_name(data_name, data_type)
        if tab_name not in self.tabs and is_in_meta(data_name, data_type):
            workspace_widget = WorkspaceWidget(data_name, data_type, self)
            workspace_widget.main_entity_widget.change_table.connect(self.change_tab)
            workspace_widget.main_entity_widget.close_tab.connect(self.remove_tab)
            self.addTab(workspace_widget, tab_name)
            self.setCurrentIndex(self.currentIndex() + 1)
        else:
            index = self.get_index(tab_name)
            self.setCurrentIndex(index) 