
from PySide2.QtWidgets import QTreeView, QVBoxLayout, QWidget, QLabel

class DatabaseExplorerWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.title_label = QLabel("Database Explorer", self)

        self.tree_view = QTreeView()

        layout = QVBoxLayout(self)
        layout.addWidget(self.title_label)
        layout.addWidget(self.tree_view)
