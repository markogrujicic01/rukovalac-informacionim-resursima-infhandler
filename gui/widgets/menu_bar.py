from PySide2.QtWidgets import QMenuBar, QAction, QMessageBox
from PySide2.QtCore import Signal

class MenuBar(QMenuBar):
    exit = Signal()
    new_file = Signal()
    open_file = Signal()
    save_file = Signal()
    close_file = Signal()
    new_db = Signal()
    open_db = Signal()
    save_db = Signal()
    close_db = Signal()
    def __init__(self, parent):
       super().__init__(parent)

       self.file_menu_button = self.addMenu("File")
       self.file_menu_button.addAction(QAction("New file", self))
       self.file_menu_button.addAction(QAction("Open file", self))
       self.file_menu_button.addAction(QAction("Save file", self))
       self.file_menu_button.addAction(QAction("Close file", self))
       self.file_menu_button.addAction(QAction("Exit", self))

       self.database_menu_button = self.addMenu("Database")
       self.database_menu_button.addAction(QAction("New database", self))
       self.database_menu_button.addAction(QAction("Open database", self))
       self.database_menu_button.addAction(QAction("Save database", self))
       self.database_menu_button.addAction(QAction("Close database", self))
       self.database_menu_button.addAction(QAction("Exit", self))

       self.help_menu = self.addMenu("Help")
       self.help_menu.addAction(QAction("Manual", self))
       self.help_menu.addAction(QAction("About", self))

       self.triggered.connect(self.actions)

    def actions(self, action):
        command = action.text()
        if command == "New file":
            self.new_file.emit()
        elif command == "Open file":
            self.open_file.emit()
        elif command == "Save file":
            self.save_file.emit()
        elif command == "Close file":
            self.close_file.emit()
        elif command == "Exit":
            self.exit.emit()
        elif command == "New database":
            self.new_db.emit()
        elif command == "Open database":
            self.open_db.emit()
        elif command == "Save database":
            self.save_db.emit()
        elif command == "Close database":
            self.close_db.emit()
        elif command == "Manual":
            QMessageBox.information(self, "Manual", "Upustvo za upotrebu aplikacije InfHandler. ")
        elif command == "About":
            QMessageBox.information(self, "About", "Projekat Rukovaoc informacionim resursima iz predmeta Specifikacija i modeliranje softvera.\n" + "Autori su Dunja Crnomarkovic i Marko Grujicic")

        