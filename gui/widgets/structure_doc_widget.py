from PySide2.QtWidgets import QDockWidget, QTreeView, QWidget, QSplitter,QTabWidget,QTabBar
from PySide2.QtWidgets import QFileSystemModel
from PySide2.QtCore import QDir, QFile, QIODevice
from PySide2.QtGui import QStandardItemModel, QStandardItem, Qt
from gui.widgets.table_view.table_view import TableView
from gui.widgets.db_explorer import DatabaseExplorerWidget
from pathlib import Path




class StructureDockWidget(QDockWidget):
    def __init__(self, title="Structure", parent=None):
        super().__init__(title, parent)
        splitter = QSplitter(Qt.Vertical, self)
        self.structure_model=QFileSystemModel()
        self.structure_model.setRootPath(QDir.currentPath())
        
        self.structure_widget = QTreeView()
        self.structure_widget.setModel(self.structure_model)
        self.structure_widget.setRootIndex(self.structure_model.index('Data'))
        
        self.explorer_widget = DatabaseExplorerWidget(self)

        splitter.addWidget(self.structure_widget)
        splitter.addWidget(self.explorer_widget)

        self.structure_widget.doubleClicked.connect(self.openSelectedFile)
        self.setWidget(splitter)

    def openSelectedFile(self, index):
        filePath = self.structure_widget.model().filePath(index)
        file_name = filePath.split("/")[-1]
        model = QStandardItemModel()
        if QFile.exists(filePath) and QFile(filePath).open(QIODevice.ReadOnly | QIODevice.Text):
            with open(filePath, "r") as file:
                for line in file:
                    row = []
                    for item in line.strip().split(';'):
                        row.append(QStandardItem(item))
                    model.appendRow(row)
                table_view = TableView(model)
            self.parent().central_widget.addTab(table_view, file_name)
            index = self.parent().central_widget.count() - 1
            self.parent().central_widget.tabBar().setTabButton(index, QTabBar.RightSide, self.parent().central_widget.close_button)
