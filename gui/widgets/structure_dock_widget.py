from PySide2.QtWidgets import QDockWidget, QTreeView, QWidget, QSplitter,QTabWidget,QTabBar
from PySide2.QtWidgets import QFileSystemModel,QVBoxLayout
from PySide2.QtCore import QDir, QFile, QIODevice, QAbstractItemModel, Signal
from PySide2.QtGui import QStandardItemModel, QStandardItem, Qt
from gui.widgets.table_view.table_view import TableView
from model.file_system_model import FileSystemModel
from model.database_explorer_model import DatabaseExplorerModel
from pathlib import Path




class StructureDockWidget(QDockWidget):
    clicked = Signal(str, str)
    def __init__(self, title, parent):
        super().__init__(title, parent)

        self.file_system_model= FileSystemModel()
        self.file_system_model.setRootPath(QDir.currentPath())
        
        self.file_system_tree = QTreeView()
        self.file_system_tree.setModel(self.file_system_model)
        self.file_system_tree.setRootIndex(self.file_system_model.index('Data'))
        
        self.db_explorer_model = DatabaseExplorerModel()
        self.db_explorer_tree = QTreeView()
        self.db_explorer_tree.setModel(self.db_explorer_model)
        
        splitter_widget = QWidget()
        layout = QVBoxLayout()
        layout.addWidget(self.file_system_tree)
        layout.addWidget(self.db_explorer_tree)
        splitter_widget.setLayout(layout)
        self.setWidget(splitter_widget)
        self.setFeatures(QDockWidget.NoDockWidgetFeatures)
        self.file_system_tree.doubleClicked.connect(self.openSelectedFile)

    def file_clicked(self, index):
        info = self.fs_model.fileInfo(index)
        if info.isFile():
            path = self.fs_model.filePath(index)
            data_type, data_name = path.split("/")[-2::]
            self.clicked.emit(data_name, data_type)

    def table_clicked(self, index):
        table_name = self.dbe_model.get_table_name(index.row())
        self.clicked.emit(table_name, "database")

    def openSelectedFile(self, index):
        filePath = self.file_system_tree.model().filePath(index)
        file_name = filePath.split("/")[-1]
        model = QStandardItemModel()
        if QFile.exists(filePath) and QFile(filePath).open(QIODevice.ReadOnly | QIODevice.Text):
            with open(filePath, "r") as file:
                for line in file:
                    row = []
                    for item in line.strip().split(';'):
                        row.append(QStandardItem(item))
                    model.appendRow(row)
                table_view = TableView(model)
            self.parent().central_widget.addTab(table_view, file_name)
