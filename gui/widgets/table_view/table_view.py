from PySide2.QtWidgets import  QTableView


class TableView(QTableView):
    def __init__(self, model):
        super().__init__()
        self.setModel(model)