from PySide2.QtWidgets import QToolBar
from PySide2.QtGui import QIcon

class Toolbar(QToolBar):
    def __init__(self,parent):
        super().__init__(parent)

        self.new_file = self.addAction(QIcon("resources/icons/new_file.png"), "New file")
        self.open_file = self.addAction(QIcon("resources/icons/open_file.png"), "Open file")
        self.save_file = self.addAction(QIcon("resources/icons/save_file.png"), "Save file")
        self.save_all = self.addAction(QIcon("resources/icons/save_all.png"), "Save all")
        self.close_file = self.addAction(QIcon("resources/icons/close_file.png"), "Close file")