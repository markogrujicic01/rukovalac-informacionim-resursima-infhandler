import json

def read_meta():
    path = "meta/meta.json"
    with open(path, "r", encoding="utf-8") as file:
        return json.load(file)