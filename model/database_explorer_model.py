from PySide2.QtCore import QStringListModel, Qt
from PySide2.QtGui import QIcon


class DatabaseExplorerModel(QStringListModel):
    def __init__(self, parent= None):
        super().__init__(parent)


    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if section == 0 and orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return "DB Explorer"
        return super().headerData(section, orientation, role)