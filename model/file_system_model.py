from PySide2.QtWidgets import QFileSystemModel
from PySide2.QtCore import  Qt

class FileSystemModel(QFileSystemModel):
    def __init__(self, parent= None):
        super().__init__(parent)
    
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if section == 0 and orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return "File Explorer"
        return super().headerData(section, orientation, role)